import * as Parse from 'parse';

export enum TrashTypeEnum {
  now,
  always
}

export default class TrashStore extends Parse.Object {
  constructor() {
    super('TrashStore');
  }

  get idRef() {
    return this.get('idRef');
  }

  set idRef(value: string) {
    this.set('idRef', value);
  }

  get classNameRef() {
    return this.get('classNameRef');
  }

  set classNameRef(value: string) {
    this.set('classNameRef', value);
  }

  get type() {
    return TrashTypeEnum[this.get('type') as string];
  }

  set type(value: TrashTypeEnum) {
    this.set('type', TrashTypeEnum[value]);
  }
}

Parse.Object.registerSubclass('TrashStore', TrashStore);
