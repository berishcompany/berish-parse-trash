import * as Parse from 'parse';

import { TrashTypeEnum } from './store';
import TrashObject from './object';

export default class TrashQuery<T extends TrashObject = TrashObject> extends Parse.Query<T> {
  constructor(objectClass: string);
  constructor(objectClass: new (...args: any[]) => T);
  constructor(objectClass: any) {
    super(objectClass);
  }

  private _includeTrash() {
    return this.include('trash');
  }

  get(objectId: string, options?: Parse.Query.GetOptions) {
    return this._includeTrash().get(objectId, options);
  }

  getInTrash(objectId: string, options?: Parse.Query.GetOptions) {
    return this._includeTrash()
      .exists('trash')
      .equalTo('trash.type', TrashTypeEnum[TrashTypeEnum.now])
      .get(objectId, options);
  }

  getNotTrash(objectId: string, options?: Parse.Query.GetOptions) {
    return this._includeTrash()
      .doesNotExist('trash')
      .get(objectId, options);
  }

  find(options?: Parse.Query.FindOptions) {
    return this._includeTrash().find(options);
  }

  findInTrash(options?: Parse.Query.FindOptions) {
    return this._includeTrash()
      .exists('trash')
      .equalTo('trash.type', TrashTypeEnum[TrashTypeEnum.now])
      .find(options);
  }

  findNotTrash(options?: Parse.Query.FindOptions) {
    return this._includeTrash()
      .doesNotExist('trash')
      .find();
  }
}
