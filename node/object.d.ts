/// <reference types="parse" />
import TrashStore from './store';
export default class TrashObject extends Parse.Object {
    constructor(className?: string, options?: any);
    constructor(attributes?: string[], options?: any);
    readonly isDeleted: boolean;
    trash: TrashStore;
    removeToTrash(): Promise<this>;
    removeFromTrash(): Promise<this>;
    restoreFromTrash(): Promise<this>;
}
