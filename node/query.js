"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const store_1 = require("./store");
class TrashQuery extends Parse.Query {
    constructor(objectClass) {
        super(objectClass);
    }
    _includeTrash() {
        return this.include('trash');
    }
    get(objectId, options) {
        return this._includeTrash().get(objectId, options);
    }
    getInTrash(objectId, options) {
        return this._includeTrash()
            .exists('trash')
            .equalTo('trash.type', store_1.TrashTypeEnum[store_1.TrashTypeEnum.now])
            .get(objectId, options);
    }
    getNotTrash(objectId, options) {
        return this._includeTrash()
            .doesNotExist('trash')
            .get(objectId, options);
    }
    find(options) {
        return this._includeTrash().find(options);
    }
    findInTrash(options) {
        return this._includeTrash()
            .exists('trash')
            .equalTo('trash.type', store_1.TrashTypeEnum[store_1.TrashTypeEnum.now])
            .find(options);
    }
    findNotTrash(options) {
        return this._includeTrash()
            .doesNotExist('trash')
            .find();
    }
}
exports.default = TrashQuery;
//# sourceMappingURL=query.js.map