/// <reference types="parse" />
import TrashStore from './store';
export default class TrashUser extends Parse.User {
    constructor(className?: string, options?: any);
    constructor(attributes?: string[], options?: any);
    get(key: string): any;
    readonly isDeleted: boolean;
    trash: TrashStore;
    removeToTrash(): Promise<this>;
    removeFromTrash(): Promise<this>;
    restoreFromTrash(): Promise<this>;
}
