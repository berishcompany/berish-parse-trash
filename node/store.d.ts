/// <reference types="parse" />
export declare enum TrashTypeEnum {
    now = 0,
    always = 1
}
export default class TrashStore extends Parse.Object {
    constructor();
    idRef: string;
    classNameRef: string;
    type: TrashTypeEnum;
}
